package com.zenika.academy.springhelloworld;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class HelloWorldConsolePrinter {

    private static Logger log = LoggerFactory.getLogger(HelloWorldConsolePrinter.class);

    public HelloWorldConsolePrinter() {
        log.info("Hello world from constructor");
    }

    @PostConstruct
    public void printHelloWorld() {
        log.info("Hello, world!");
    }
}
